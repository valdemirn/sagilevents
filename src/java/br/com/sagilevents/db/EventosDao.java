/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.db;

import br.com.sagilevents.util.ConexaoDB;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import br.com.sagilevents.bean.Eventos;
import br.com.sagilevents.bean.Usuario;
import java.sql.Statement;

public class EventosDao {
    
     private final Connection c;
    //metodo de conexao como o banco de dados
    public EventosDao() throws SQLException, ClassNotFoundException{
        this.c = new ConexaoDB().getConnection();
    }
    
    public Eventos insereEvento(Eventos ev) throws SQLException{
      String sql = "insert into eventos" + "(nome_evento, email_evento, data_inicio_evento, data_fim_evento, usuarios_id)" + "values (?,?,?,?,?)";
       // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql); //inseri os valores no banco de dados 

        // seta os valores
        stmt.setString(1,ev.getNome());
        stmt.setString(2,ev.getEmail());
        stmt.setString(3,ev.getDataInicio());
        stmt.setString(4,ev.getDataFim());
        stmt.setInt(5,ev.getIdUsuario());
       
        // executa
        stmt.execute();
        stmt.close();
        return ev;
    }
    
      public List<Eventos> listaEv(Eventos leventos) throws SQLException{
        //usus: array armazena a lista de registros
        List<Eventos> evento = new ArrayList<Eventos>();
        
        String sql = "SELECT * from eventos where usuarios_id = ?";
        PreparedStatement stmt = this.c.prepareStatement(sql);
        // seta os valores
        stmt.setInt(1,leventos.getIdUsuario());
       
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {      
            // criando o objeto Eventos
            Eventos ev = new Eventos(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5)
            );
            // adiciona o usu à lista de eventos
            evento.add(ev);
        }
        
        rs.close();
        stmt.close();
        return evento;
        
    }
      
    public Eventos exclui(Eventos eve) throws SQLException{
        String sql = "delete from eventos WHERE ideventos = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setInt(1,eve.getId());
        // executa
        stmt.execute();
        stmt.close();
        c.close();
        return eve;
    } 
    
    
        public Eventos busca(Eventos eve) throws SQLException{
        String sql = "select * from eventos WHERE ideventos = ?";
        
        // seta os valores
        try ( // prepared statement para inserção
             PreparedStatement stmt = this.c.prepareStatement(sql)) {
            // seta os valores
            stmt.setInt(1,eve.getId());
            // executa
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                // criando o objeto Usuario
                eve.setId(rs.getInt(1));
                eve.setNome(rs.getString(2));
                eve.setEmail(rs.getString(3));
                eve.setDataInicio(rs.getString(4));
                eve.setDatafim(rs.getString(5));
            }
        }
        return eve;
    }
    
     public Eventos altera(Eventos eve) throws SQLException{
        String sql = "UPDATE eventos SET nome_evento = ?, email_evento = ?, data_inicio_evento = ?, data_fim_evento = ? WHERE ideventos = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,eve.getNome());
        stmt.setString(2,eve.getEmail());
        stmt.setString(3,eve.getDataInicio());
        stmt.setString(4,eve.getDataFim());
        // executa
        stmt.execute();
        stmt.close();
        return eve;
    }
         
}
