
package br.com.sagilevents.db;

import br.com.sagilevents.util.ConexaoDB;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import br.com.sagilevents.bean.Usuario;
import br.com.sagilevents.bean.PessoaFisica;
import br.com.sagilevents.bean.PessoaJuridica;
import java.sql.Statement;

public class UsuarioDao {

    private final Connection c;
    //metodo de conexao como o banco de dados
    public UsuarioDao() throws SQLException, ClassNotFoundException{
        this.c = new ConexaoDB().getConnection();
    }
    
    //metodo de validacao de Login   
    public Usuario validaLogin(Usuario usu) throws SQLException{
           // cria o select para ser executado no banco de dados 
        String sql = "select * from usuarios WHERE login = ? AND senha = ?";
        // prepared statement para seleção, preparando a execucao desse select na memoria
        PreparedStatement stmt = this.c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,usu.getLogin());
        stmt.setString(2,usu.getSenha());
        // executa, ResultSet = estrutura da memoria percorre esa estrutura lendo  
        //cada item da estrutura e seta os valores nos  beans, retornaando
        //informacao para o jsp que solicito a informacao
        ResultSet rs = stmt.executeQuery();
        // o while percorrendo o result Set = rs
        while (rs.next()) {      
            // criando o objeto Usuario os numero coincidem com as colunas no SQL
            usu.setId(rs.getInt(1));
            usu.setNome(rs.getString(2));
            usu.setLogin(rs.getString(3));
            usu.setSenha(rs.getString(4));
            usu.setStatus(rs.getString(5));
            usu.setEmail(rs.getString(6));
            usu.setTipo(rs.getString(7));
            usu.setIsperson(rs.getString(8));
            // adiciona o usu à lista de usus
        }
        stmt.close();
       
        return usu;
        //retorna os capmpo que foram setados com valores no campo '
    }
    
    
     public Usuario inseri(Usuario usu) throws SQLException{
        String sql = "insert into usuarios" + "(nome, login, senha, status,email, tipo, isperson)" + "values (?,?,?,?,?,?,?)";
    
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS); //inseri os valores no banco de dados 

        // seta os valores
        stmt.setString(1,usu.getNome());
        stmt.setString(2,usu.getLogin());
        stmt.setString(3,usu.getSenha());
        stmt.setString(4,usu.getStatus());
        stmt.setString(5,usu.getEmail());
        stmt.setString(6,usu.getTipo());
        stmt.setString(7,usu.getIsperson());

        // executa
        stmt.executeUpdate();
        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                usu.setId(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        }      
            stmt.close();
        return usu;
    }
    
      public PessoaFisica inseripf(PessoaFisica pf) throws SQLException{
      String sql = "insert into pessoas" + "(idUsuario, nome, cpf, data, tipo, email, fone, celfone, rua, numero, bairro, cidade, uf, cep, isperson)" + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
       // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql); //inseri os valores no banco de dados 

        // seta os valores
        stmt.setInt(1,pf.getIdUsuario());
        stmt.setString(2,pf.getNome());
        stmt.setString(3,pf.getCpf());
        stmt.setString(4,pf.getDatanasc());
        stmt.setString(5,pf.getTipo());
        stmt.setString(6,pf.getEmail());
        stmt.setString(7,pf.getFone());
        stmt.setString(8,pf.getCelfone());
        stmt.setString(9,pf.getRua());
        stmt.setString(10,pf.getNumero());
        stmt.setString(11,pf.getBairro());
        stmt.setString(12,pf.getCidade());
        stmt.setString(13,pf.getUf());
        stmt.setString(14,pf.getCep());
        stmt.setString(15,pf.getIsperson());
        // executa
        stmt.execute();
        stmt.close();
        return pf;
    }
     public PessoaJuridica inseripj(PessoaJuridica pj) throws SQLException{
         
                                                             
      String sql = "insert into pessoajuridica" + "(idUsuario, nome, razaosocial,  inscestadual , cnpj, email, fone,  celfone,  rua,  numero,  bairro,  cidade,  uf,  cep, isperson)" + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
       // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql); //inseri os valores no banco de dados 
        // seta os valores
        stmt.setInt(1,pj.getIdUsuario());
        stmt.setString(2,pj.getNome());
        stmt.setString(3,pj.getRazaosocial());
        stmt.setString(4,pj.getInscestadual());
        stmt.setString(5,pj.getCnpj());
        stmt.setString(6,pj.getEmail());
        stmt.setString(7,pj.getFone());
        stmt.setString(8,pj.getCelfone());
        stmt.setString(9,pj.getRua());
        stmt.setString(10,pj.getNumero());
        stmt.setString(11,pj.getBairro());
        stmt.setString(12,pj.getCidade());
        stmt.setString(13,pj.getUf());
        stmt.setString(14,pj.getCep());
        stmt.setString(15,pj.getIsperson());
        // executa
        stmt.execute();
        stmt.close();
        return pj;
    }
      
      
      
    public Usuario busca(Usuario usu) throws SQLException{
        String sql = "select * from usuarios WHERE id = ?";
        
        // seta os valores
        try ( // prepared statement para inserção
             PreparedStatement stmt = this.c.prepareStatement(sql)) {
            // seta os valores
            stmt.setInt(1,usu.getId());
            // executa
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                // criando o objeto Usuario
                usu.setId(rs.getInt(1));
                usu.setNome(rs.getString(2));
                usu.setLogin(rs.getString(3));
                usu.setSenha(rs.getString(4));
                usu.setStatus(rs.getString(5));
                usu.setEmail(rs.getString(6));
                usu.setTipo(rs.getString(7));
                usu.setIsperson(rs.getString(8));
                // adiciona o usu à lista de usus
            }
        }
        return usu;
    }
    
    
    
    public Usuario altera(Usuario usu) throws SQLException{
        String sql = "UPDATE usuarios SET nome = ?, login = ?, senha = ?, status = ?, tipo = ?, isperson = ? WHERE id = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,usu.getNome());
        stmt.setString(2,usu.getLogin());
        stmt.setString(3,usu.getSenha());
        stmt.setString(4,usu.getStatus());
        stmt.setString(5,usu.getTipo());
        stmt.setInt(6,usu.getId());
        stmt.setString(6,usu.getIsperson());

        // executa
        stmt.execute();
        stmt.close();
        return usu;
    }

    public Usuario exclui(Usuario usu) throws SQLException{
        String sql = "delete from usuarios WHERE id = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setInt(1,usu.getId());
        // executa
        stmt.execute();
        stmt.close();
        c.close();
        return usu;
    }
 
  //o metodo que esta dentro do DaO e o metodo 'listas' ele retorna um Array 'List' de Usuario
    // sumete o usuEnt de usuario
    public List<Usuario> lista(Usuario usuEnt) throws SQLException{
        //usus: array armazena a lista de registros
        List<Usuario> usus = new ArrayList<Usuario>();
        
        String sql = "select u.id, a.nome from pessoas a join usuarios u on u.id = a.idUsuario where a.nome LIKE ? union all select u.id, b.nome from pessoajuridica b join usuarios u on u.id = b.idUsuario where b.nome LIKE ?; ";
        PreparedStatement stmt = this.c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,"%" + usuEnt.getNome() + "%");
        stmt.setString(2,"%" + usuEnt.getNome() + "%");
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()) {      
            // criando o objeto Usuario
            Usuario usu = new Usuario(
                rs.getInt(1),
                rs.getString(2)
            );
            // adiciona o usu à lista de usus
            usus.add(usu);
        }
        
        rs.close();
        stmt.close();
        return usus;
        
    }
    
    public PessoaFisica buscaPf(PessoaFisica pf) throws SQLException{
        String sql = "select * from pessoas WHERE idUsuario = ?";
        
        // seta os valores
        try ( // prepared statement para inserção
             PreparedStatement stmt = this.c.prepareStatement(sql)) {
            // seta os valores
            stmt.setInt(1,pf.getId());
            // executa
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                pf.setId(rs.getInt(1));
                pf.setIdUsuario(rs.getInt(2));
                pf.setNome(rs.getString(3));
                pf.setCpf(rs.getString(4));
                pf.setDatanasc(rs.getString(5));
                pf.setTipo(rs.getString(6)); 
                pf.setEmail(rs.getString(7));
                pf.setStatus(rs.getString(8));     
                pf.setFone(rs.getString(9));
                pf.setCelfone(rs.getString(10));
                pf.setRua(rs.getString(11));
                pf.setNumero(rs.getString(12));
                pf.setBairro(rs.getString(13));
                pf.setCidade(rs.getString(14));
                pf.setUf(rs.getString(15));
                pf.setCep(rs.getString(16));
                pf.setIsperson(rs.getString(17));
            }
        }
        return pf;
    }
    
      public PessoaFisica atualizaPf(PessoaFisica pf) throws SQLException{
        String sql = "UPDATE pessoas SET nome = ?, cpf = ?, data = ?, tipo = ?, email = ?, stattus = ?, fone = ?, celfone = ?, rua = ?,	numero = ?, bairro = ?,	cidade = ?, uf = ?, cep = ?, isperson = ? WHERE id = ?";
           
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores   
        stmt.setString(1,pf.getNome());
        stmt.setString(2,pf.getCpf());
        stmt.setString(3,pf.getDatanasc());
        stmt.setString(4,pf.getTipo()); 
        stmt.setString(5,pf.getEmail());
        stmt.setString(6,pf.getStatus());     
        stmt.setString(7,pf.getFone());
        stmt.setString(8,pf.getCelfone());
        stmt.setString(9,pf.getRua());
        stmt.setString(10, pf.getNumero());
        stmt.setString(11,pf.getBairro());
        stmt.setString(12,pf.getCidade());
        stmt.setString(13,pf.getUf());
        stmt.setString(14, pf.getCep());
        stmt.setString(15, pf.getIsperson());
        stmt.setInt(16, pf.getId());

        // executa
        stmt.execute();
        stmt.close();
        return pf;
    }
    
    
      public PessoaJuridica buscaPj(PessoaJuridica pj) throws SQLException{
        String sql = "select * from pessoajuridica WHERE idUsuario = ?";
        
        // seta os valores
        try ( // prepared statement para inserção
             PreparedStatement stmt = this.c.prepareStatement(sql)) {
            // seta os valores
            stmt.setInt(1,pj.getId());
            // executa
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                pj.setId(rs.getInt(1));
                pj.setIdUsuario(rs.getInt(2));
                pj.setNome(rs.getString(3));
                pj.setRazaoSocial(rs.getString(4));
                pj.setInscestadual(rs.getString(5));
                pj.setCnpj(rs.getString(6));
                pj.setEmail(rs.getString(7)); 
                pj.setFone(rs.getString(8));
                pj.setCelfone(rs.getString(9));
                pj.setRua(rs.getString(10));
                pj.setNumero(rs.getString(11));
                pj.setBairro(rs.getString(12));
                pj.setCidade(rs.getString(13));
                pj.setUf(rs.getString(14));
                pj.setCep(rs.getString(15));
                pj.setIsperson(rs.getString(16));
            }
        }
        return pj;
    }
      
      
   public PessoaJuridica atualizaPj(PessoaJuridica pj) throws SQLException{
        String sql = "UPDATE pessoajuridica SET nome = ?, razaosocial = ?, inscestadual = ?, cnpj = ?, email = ?, fone = ?, celfone = ?, rua = ?, numero = ?, bairro = ?, cidade = ?, uf = ?, cep = ?, isperson = ? WHERE idpessoajuridica = ?";
           
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores   
        stmt.setString(1,pj.getNome());
        stmt.setString(2,pj.getRazaosocial()); 
        stmt.setString(3,pj.getInscestadual()); 
        stmt.setString(4,pj.getCnpj()); 
        stmt.setString(5,pj.getEmail());   
        stmt.setString(6,pj.getFone());
        stmt.setString(7,pj.getCelfone());
        stmt.setString(8,pj.getRua());
        stmt.setString(9, pj.getNumero());
        stmt.setString(10,pj.getBairro());
        stmt.setString(11,pj.getCidade());
        stmt.setString(12,pj.getUf());
        stmt.setString(13, pj.getCep());
        stmt.setString(14, pj.getIsperson());
        stmt.setInt(15, pj.getId());

        // executa
        stmt.execute();
        stmt.close();
        return pj;
    }   
    
}







/*
    
    
    /*
    /*public Usuario inseri(Usuario usu) throws SQLException{
        //cria o select 
        String sql = "insert into usuarios"+"(nome,login,senha,status,tipo)"+"values(?,?,?,?,?)";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,usu.getNome());
        stmt.setString(2,usu.getLogin());
        stmt.setString(3,usu.getSenha());
        stmt.setString(4,usu.getStatus());
        stmt.setString(5,usu.getTipo());
        // executa
        System.out.println(sql);
        stmt.execute();
        stmt.close();
        return usu;
         }*/
    /*
    public Usuario exclui(Usuario usu) throws SQLException{
        String sql = "delete from usuarios WHERE id = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setInt(1,usu.getId());
        // executa
        stmt.execute();
        stmt.close();
        c.close();
        return usu;
    }
    
    public Usuario altera(Usuario usu) throws SQLException{
        String sql = "UPDATE usuarios SET nome = ?, login = ?, senha = ?, status = ?, tipo = ? WHERE id = ?";
        // prepared statement para inserção
        PreparedStatement stmt = c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,usu.getNome());
        stmt.setString(2,usu.getLogin());
        stmt.setString(3,usu.getSenha());
        stmt.setString(4,usu.getStatus());
        stmt.setString(5,usu.getTipo());
        stmt.setInt(6,usu.getId());

        // executa
        stmt.execute();
        stmt.close();
        return usu;
    }

    
    public Usuario validaLogin(Usuario usu) throws SQLException{
        //cria o select para ser executado no banco de dados
        String sql = "select * from usuarios WHERE login = ? AND senha = ?";
        // prepared statement para seleçao, colocando esse select na memoria
        PreparedStatement stmt = this.c.prepareStatement(sql);
        // seta os valores
        stmt.setString(1,usu.getLogin());
        stmt.setString(2,usu.getSenha());
        // executa
        ResultSet rs = stmt.executeQuery();
        //percorre o result sset rs
        while (rs.next()) {      
            // criando o objeto Usuario
            usu.setId(rs.getInt(1));
            usu.setNome(rs.getString(2));
            usu.setLogin(rs.getString(3));
            usu.setSenha(rs.getString(4));
            usu.setStatus(rs.getString(5));
            usu.setTipo(rs.getString(6));
            // adiciona o usu à lista de usus
        }
        stmt.close();
        return usu;
    }
    
}
*/