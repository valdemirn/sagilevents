
package br.com.sagilevents.controler;

import br.com.sagilevents.bean.Usuario;
import br.com.sagilevents.bean.PessoaFisica;
import br.com.sagilevents.bean.PessoaJuridica;
import br.com.sagilevents.db.UsuarioDao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

 public class UsuarioControler {
    
    public Usuario validaUsuario(Usuario usu) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        usu = usuDao.validaLogin(usu);
        return usu;
    }
    
    public Usuario validaPessoaFisica(Usuario pf) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        pf = usuDao.validaLogin(pf);
        return pf;
    }
     
    public PessoaFisica inserirPessoaFisica( PessoaFisica pf) throws SQLException, ClassNotFoundException {
       UsuarioDao usuDao = new UsuarioDao();
       pf = usuDao.inseripf(pf);
      
       return pf;  
    }
     public PessoaJuridica inserirPessoaJuridica( PessoaJuridica pj) throws SQLException, ClassNotFoundException {
       UsuarioDao usuDao = new UsuarioDao();
       pj = usuDao.inseripj(pj);
      
       return pj;  
    }
    //Metodo inserir usuario
     public Usuario inserirUsuario( Usuario usu) throws SQLException, ClassNotFoundException {
       UsuarioDao usuDao = new UsuarioDao();
       usu = usuDao.inseri(usu);
       return usu;  
    }

    public List<Usuario> listarUsuario(Usuario usu) throws SQLException, ClassNotFoundException {
       List<Usuario> usus ;
       UsuarioDao usuDao = new UsuarioDao();
       usus = usuDao.lista(usu);
       return usus;
    }
    
  public Usuario buscarUsuario(Usuario usu) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        usu = usuDao.busca(usu);
        /*PessoaControler pesCont = new PessoaControler(); 
        usu.setPes(pesCont.buscaPessoaFisica());*/
        return usu;
    } 

    public Usuario alterarUsuario(Usuario usu) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        usu = usuDao.altera(usu);
        return usu;
    }
    
    public Usuario excluirUsuario(Usuario usu) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        usu = usuDao.exclui(usu);
        return usu;
    }
    public PessoaFisica buscaPessoaFisica(PessoaFisica pf) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        pf = usuDao.buscaPf(pf);
        /*PessoaControler pesCont = new PessoaControler;;;(); 
        usu.setPes(pesCont.buscaPessoaFisica());*/
        return pf;
    }
    
    public PessoaFisica alterarPf(PessoaFisica pf) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        pf = usuDao.atualizaPf(pf);
        return pf;
    }
    
    
    public PessoaJuridica buscaPessoaJuridica(PessoaJuridica pj) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        pj = usuDao.buscaPj(pj);
        /*PessoaControler pesCont = new PessoaControler;;;(); 
        usu.setPes(pesCont.buscaPessoaFisica());*/
        return pj;
    }
    
    public PessoaJuridica alterarPj(PessoaJuridica pj) throws SQLException, ClassNotFoundException {
        UsuarioDao usuDao = new UsuarioDao();
        pj = usuDao.atualizaPj(pj);
        return pj;
    }
 
 
 }

