/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.controler;

import br.com.sagilevents.bean.Eventos;
import br.com.sagilevents.db.EventosDao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class EventosController {
    
    public Eventos insereEvento(Eventos evento)throws SQLException, ClassNotFoundException{
        
        EventosDao eventoDao = new EventosDao();
        evento = eventoDao.insereEvento(evento);
        return evento;
    }
    
    public List<Eventos> listarEventos(Eventos ev) throws SQLException, ClassNotFoundException {
       List<Eventos> eve ;
       EventosDao evDao = new EventosDao();
       eve = evDao.listaEv(ev);
       return eve;
    }
    
    public Eventos excluiEvento(Eventos  evento) throws SQLException, ClassNotFoundException {
        EventosDao evDao = new EventosDao();
        evento = evDao.exclui(evento);
        return evento;
    }
    
    public Eventos buscaEvento(Eventos evento) throws SQLException, ClassNotFoundException {
         EventosDao evDao = new EventosDao();
        evento = evDao.busca(evento);
        return evento;
    }
    
     public Eventos alterarEvento(Eventos evento) throws SQLException, ClassNotFoundException {
        EventosDao evDao = new EventosDao();
        evento = evDao.altera(evento);
        return evento;
    }
}

  