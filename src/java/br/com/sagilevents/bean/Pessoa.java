/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.bean;



public class  Pessoa{
  	
  
 private int id;
    private String nome; 
    private String status;
    private String tipo;
    private String email;
    private String fone;
    private String celfone;
    private String rua;
    private String numero;
    private String bairro;
    private String cidade;
    private String uf;
    private String cep;
    private String isperson;
    
public Pessoa (int idp, String nomep,  String emailp, String fonep, String celfonep, String ruap, String numerop, String bairrop, String cidadep, String ufp, String cepp, String ispersonp ) {
        this.id = idp;
        this.nome = nomep;
        this.email = emailp;
        this.fone = fonep ;
        this.celfone  = celfonep;
        this.rua  =  ruap;
        this.numero  = numerop ;
        this.bairro  = bairrop;
        this.cidade  = cidadep;
        this.uf  = ufp ;
        this.cep  = cepp ;
        this.isperson  = ispersonp ;
                                  
        
    }
   
    //metodos de manipulacao gets e sets 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
  
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
   
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }    
     /**
     * @return the fone
     */
    public String getFone() {
        return fone;
    }

    /**
     * @param fone the fone to set
     */
    public void setFone(String fone) {
        this.fone = fone;
    }

    /**
     * @return the celfone
     */
    public String getCelfone() {
        return celfone;
    }

    /**
     * @param celfone the celfone to set
     */
    public void setCelfone(String celfone) {
        this.celfone = celfone;
    }

    /**
     * @return the rua
     */
    public String getRua() {
        return rua;
    }

    /**
     * @param rua the rua to set
     */
    public void setRua(String rua) {
        this.rua = rua;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * @return the cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(String cep) {
        this.cep = cep;
    }
 
    public String getIsperson() {
        return isperson;
    }
    public void setIsperson(String isperson) {
        this.isperson = isperson;
    }
    
}