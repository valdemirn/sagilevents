/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.bean;


public class PessoaFisica extends Pessoa{
    
    private String cpf;
    private String datanasc;
    private String sexo;
    private int idUsuario;
    private String isperson;

    public PessoaFisica(int idp, String nomep, String emailp, String cpfp, String datanascp, String fonep, String sexop,  String celfonep, String ruap, String numerop, String bairrop, String cidadep, String ufp, String cepp,String ispersonp) {
    super (idp, nomep, emailp,  fonep, celfonep, ruap,  numerop, bairrop, cidadep,  ufp, cepp, ispersonp );
   
    this.cpf = cpfp;
    this.datanasc = datanascp;
    this.sexo = sexop;
    this.isperson = ispersonp;
     }
    
         public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the datanasc
     */
    public String getDatanasc() {
        return datanasc;
    }

    /**
     * @param datanasc the datanasc to set
     */
    public void setDatanasc(String datanasc) {
        this.datanasc = datanasc;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario){
        this.idUsuario = idUsuario;
    }
    
    public String getIsperson() {
        return isperson;
    }

    public void setIsperson(String isperson){
        this.isperson = isperson;
    }
}
    

  
