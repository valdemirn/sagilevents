
package br.com.sagilevents.bean;

public class Usuario{
    
    private int id;
    private String nome;
    private String login;
    private String senha;
    private String status;
    private String email;
    private String tipo;
    private String isperson;
  
    //metodo construror
    public Usuario (int idp, String nomep,String loginp, String senhap, String statusp, String emailp, String tipop) {
        this.id = idp;
        this.nome = nomep;
        this.login = loginp;
        this.senha = senhap;
        this.status = statusp;
        this.email = emailp;
        this.tipo = tipop;    
    }

    public Usuario (int idp, String nomep){
        this.id =idp;
        this.nome = nomep;
    }
   
    //metodos de manipulacao gets e sets 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
        public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getStatus() {
        return status; 
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


    public void setIsperson(String isperson){
        this.isperson = isperson;
    }
    
    public String getIsperson(){
        return this.isperson;
    }
}
