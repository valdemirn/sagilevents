/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.bean;


public class PessoaJuridica extends Pessoa{
    private String razaosocial;
    private String inscestadual;
    private String cnpj;
    private int idUsuario;
    private String isperson;

                             
    public PessoaJuridica(int idp, String nomep, String statusp, String tipop, String emailp, String razaosocialp, String inscestadualp , String cnpjp, String fonep, String celfonep, String ruap, String numerop, String bairrop, String cidadep, String ufp, String cepp, String ispersonp) {
    super (idp, nomep, emailp,  fonep, celfonep, ruap,  numerop, bairrop, cidadep,  ufp, cepp, ispersonp );   
    this.razaosocial = razaosocialp;
    this.inscestadual = inscestadualp;
    this.cnpj = cnpjp;
    this.isperson = ispersonp;
    }
    
    public String getRazaosocial() {
        return razaosocial;
     }
    public void setRazaoSocial(String razaosocial) {
        this.razaosocial = razaosocial;
    }
    
    public String getInscestadual() {
        return inscestadual;
    }
    public void setInscestadual(String inscestadual) {
        this.inscestadual = inscestadual;
    }
    
    public String getCnpj() {
        return cnpj;
    }
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
     public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario){
        this.idUsuario = idUsuario;
    }
    @Override
        public String getIsperson() {
        return isperson;
    }

    @Override
    public void setIsperson(String isperson){
        this.isperson = isperson;
    }
}
    