/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sagilevents.bean;

public class Eventos {
   private int id;
   private int idUsuario;
   private String nome;
   private String email;
   private String dataInicio;
   private String dataFim;
   
   public Eventos(int idUsuario, String nome, String email, String dataInicio, String dataFim){
       this.idUsuario = idUsuario;
       this.nome = nome;
       this.email = email;
       this.dataInicio = dataInicio;
       this.dataFim = dataFim;
   }
   
  public Eventos(int id, int idUsuario){
      this.id = id;
      this.idUsuario = idUsuario;
  } 
   
 public Eventos(int idUsuario){
     this.idUsuario = idUsuario;
 };

    public int getId() {
        return id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }
   
   public void setId(int id){
       this.id = id;
   }
              
                
  public void setNome(String nome){
       this.nome = nome;
   }
    
 public void setEmail(String email){
       this.email = email;
  }
 
 public void setDataInicio(String dataInicio){
       this.dataInicio = dataInicio;
 }
 
 public void setDatafim(String dataFim){
       this.dataFim = dataFim;
   }
                
                
}
