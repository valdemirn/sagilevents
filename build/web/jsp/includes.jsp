<%-- 
    Document   : includes
    Created on : 12/06/2018, 15:25:41
    Author     : valdemir
--%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.VerificaEvento"%>
<%@page import="br.com.sagilevents.bean.Eventos"%>
<%@page import="br.com.sagilevents.controler.EventosController"%>
<%@page import ="java.util.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  Usuario usuLogado = (Usuario) session.getAttribute("UsuarioLogado");
  VerificaEvento verif = new VerificaEvento();
  String label = "";
  String url = "";
  String conta = "";
  
 if(usuLogado.getIsperson().equals("fisica")){
       conta = "../minaconta/mcfisica.jsp";
    }else{
       conta = "../minaconta/mcjuridica.jsp";  
 }    
  
if(verif.possuiEvento(usuLogado.getId())){
       label = "Gerenciar Eventos";
       url = "../evento/meusEventos.jsp";
    }else{
       label ="Criar Evento";
       url = "../evento/criarEvento.jsp";
     }
%>