<%-- 
    Document   : validaExcluiEvento
    Created on : 12/06/2018, 16:04:24
    Author     : valdemir
--%>
<%@page import="br.com.sagilevents.controler.EventosController"%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.Eventos"%>

<%
 request.getParameterValues("");
 String codigo = request.getParameter("ID");
 int id = Integer.parseInt(codigo);
 Usuario usuLogado = (Usuario) session.getAttribute("UsuarioLogado"); 
 int idUsuario = usuLogado.getId();

Eventos evento = new Eventos(id, idUsuario);

EventosController eventoController = new EventosController();
eventoController.excluiEvento(evento);
response.sendRedirect("meusEventos.jsp");
    
%>