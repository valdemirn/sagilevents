<%-- 
    Document   : meusEventos1
    Created on : 12/06/2018, 11:19:27
    Author     : valdemir
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.Eventos"%>
<%@page import="br.com.sagilevents.controler.EventosController"%>
<%@page import ="java.util.*"%>

<%
Usuario usuarioLogado = (Usuario) session.getAttribute("UsuarioLogado"); 
int idUsuario = usuarioLogado.getId();
Eventos evento = new Eventos(idUsuario);
EventosController evcount = new EventosController();
List<Eventos> eventos = evcount.listarEventos(evento);
String urls = "ID=";
%>
<!DOCTYPE html>
<html>
<head>
 <%@include file="../header.jsp"%>
</head>
<%@include file="../includes.jsp"%>
<%@include file="../navigation.jsp"%>
  <!-- Main content -->
   <!--<div class="col-md-6 col-centered">-->
       <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista de Eventos</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <th>Nome do Evento</th>
                      <th>Email</th>
                      <th>Data Inicio</th>
                      <th>Data Término</th>
                       <% if (usuarioLogado.getTipo().equals("ADM")) { %>
                      <th>Alterar</th>
                      <th>Exclui</th>
                       <% } %>
                    </tr>
                </thead>
                <% if (!(eventos.isEmpty())) { %>   
                <tbody>
                    <% for (Eventos listaEventos : eventos) { %> 
                    <tr>
                        <td><%=listaEventos.getNome()%></td>
                        <td><%=listaEventos.getEmail()%></td>
                        <td><%=listaEventos.getDataInicio()%></td>
                        <td><%=listaEventos.getDataFim()%></td>
                        <% if (usuarioLogado.getTipo().equals("ADM")) { %>
                            <td><a href="validaExcluiEvento.jsp?<%=urls + listaEventos.getIdUsuario()%>">Excluir</a></td>
                            <td><a href="alteraEvento.jsp?<%=urls + listaEventos.getIdUsuario()%>">Alterar</a></td>
                        <% } %>
                    </tr>
                        <% } %>
                </tbody>
                <% } %>
              </table>
            </div>
            <!-- /.card-body -->
         </div>  
 <!-- </div>-->
<%@include file="../footer.jsp"%>
<script src="../../inc/plugins/datatables/jquery.dataTables.js"></script>
<script src="../../inc/plugins/datatables/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
