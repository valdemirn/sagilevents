<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.controler.UsuarioControler"%>
<%@ page session="true"%>

<%
  String login = request.getParameter("EMAIL");
  String senha = request.getParameter("SENHA");
   
  Usuario usu = new Usuario(0,"",login,senha,"","","");
   
  UsuarioControler usucont = new UsuarioControler();
  usu = usucont.validaUsuario(usu);
  session.setAttribute("UsuarioLogado",usu);
%>

<!-- DOCTYPE html-->
<html>
    <%@include file="../../inc/materalizeWeb.inc" %>
    <title>SAGIL MANTER USUARIOS</title>
   
    <body>  
        <% if (!usu.getStatus().equals("")) { %>
            <!-- Dropdown1 Trigger -->
           
            <a class='dropdown-button btn' data-beloworigin="true" href='#' data-activates='dropdown1'>SAGIL Controle de Usuários</a>  
            <a class='dropdown-button btn' data-beloworigin="true" href='#' data-activates='dropdown2'>SAGIL Administrar Eventos</a>
            <% if (usu.getTipo().equals("ADM")) { %>
                <!-- Dropdown1 Structure -->
                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="../usuario/inserirUsuario.jsp"> Inserir Usuario </a></li>
                    <li><a href="../usuario/consultarUsuario.jsp"> Consultar Usuarios </a></li>
                </ul>
            <% } else { %>
                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="../usuario/consultarUsuario.jsp"> Consultar Usuarios </a></li>
                </ul>
            <% } %>
            <% if (usu.getTipo().equals("ADM")) { %>
                <!-- Dropdown2 Structure -->
                <ul id='dropdown2' class='dropdown-content'>
                    <li><a href="../eventos/criarEvento.jsp">Criar Evento </a></li>
                    <li><a href="../eventos/meusEventos.jsp"> Listar Eventos </a></li>
                </ul>
            <% } else { %>
                <ul id='dropdown2' class='dropdown-content'>
                    <li><a href="../eventos/meusEventos.jsp"> Consultar Eventos </a></li>
                </ul>
            <% } %>
        <% } else { %>
                <h1>USUÁRIO INVÁLIDO</h1>
        <% } %>
    </body>
</html>