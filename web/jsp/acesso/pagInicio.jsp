<%-- 
    Document   : pagInicio
    Created on : 11/06/2018, 16:21:00
    Author     : valdemir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <%@include file="../header.jsp"%>
 <style> 
     .container {
  position: relative;
  height: 14rem;
  border: 1px solid;
}

.jumbotron {
  position: absolute;
  top: 50%;
  left:50%;
  /*transform: translate(-50%,-50%);*/
  border: 1px dashed deeppink;
}
 </style>
</head>
<%@include file="../includes.jsp"%>
<%@include file="../navigation.jsp"%>
  <!-- Main content -->
     <div class="col-lg-4 col-centered center">
          <button onclick="location.href='<%=url%>'"  type="button" class="btn btn-block btn-outline-warning btn-lg"><%=label%></button>   
     </div>   
<%@include file="../footer.jsp"%>
