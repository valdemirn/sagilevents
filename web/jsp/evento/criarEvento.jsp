<%-- 
    Document   : criarEvento1
    Created on : 11/06/2018, 18:22:00
    Author     : valdemir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <%@include file="../header.jsp"%>
</head>
<%@include file="../includes.jsp"%>
<%@include file="../navigation.jsp"%>
  <!-- Main content -->
   <div class="col-md-6 col-centered">
     <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Adicionar Eventos</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" name="criaEvento" id="criaEvento" action="validaCriaEvento.jsp" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomeEvento">Nome Evento</label>
                    <input type="text" type="text" name="nome_evento" value="" class="form-control" id="nomeEvento" placeholder="Nome do evento">
                  </div>
                  <div class="form-group">
                    <label for="email_evento">Email address</label>
                    <input type="email" class="form-control" id="email_evento" placeholder="Enter email" name="email_evento" value=""   >
                  </div>
                  <div class="form-group">
                  <label>Data Inicio:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="data_inicio" value="">
                  </div>
                  <!-- /.input group -->
                 </div>
                 <div class="form-group">
                    <label>Data Término:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                            <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="data_fim" value="">
                        </div>
                  <!-- /.input group -->
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button  onclick="location.href='../acesso/pagInicio.jsp'"  type="button"  class="btn btn-danger">cancelar</button>
                </div>
              </form>
            </div>
        </div>

<%@include file="../footer.jsp"%>