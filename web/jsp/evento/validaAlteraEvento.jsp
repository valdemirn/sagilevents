<%-- 
    Document   : validaAlteraEvento
    Created on : 12/06/2018, 16:50:58
    Author     : valdemir
--%>
<%@page import="br.com.sagilevents.controler.EventosController"%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.Eventos"%>

<%
  request.getParameterValues("");
  String cod = request.getParameter("ID");
  int idEvento = Integer.parseInt(cod);
  String nome_evento = request.getParameter("nome_evento");
  String email_evento = request.getParameter("email_evento");
  String data_inicio = request.getParameter("data_inicio");
  String data_fim = request.getParameter("data_fim");
  
Usuario usuLogado = (Usuario) session.getAttribute("UsuarioLogado"); 
int idUsuario = usuLogado.getId();

Eventos evento = new Eventos(idUsuario,nome_evento,email_evento,data_inicio,data_fim);
evento.setId(idEvento);

EventosController eventoController = new EventosController();
eventoController.alterarEvento(evento);
response.sendRedirect("meusEventos.jsp");
    
%>
