<%-- 
    Document   : mcfisica1
    Created on : 12/06/2018, 13:17:29
    Author     : valdemir
--%>
<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.PessoaFisica"%>
<%@page import="br.com.sagilevents.controler.UsuarioControler"%>
<%@page import ="java.util.*"%>

<%
UsuarioControler usucont = new UsuarioControler();
Usuario usuarioLogado = (Usuario) session.getAttribute("UsuarioLogado");
PessoaFisica pf = new PessoaFisica (0,"","","","","","","","","","","","","","");
pf.setId(usuarioLogado.getId());
UsuarioControler usucount = new UsuarioControler();
PessoaFisica psf = usucount.buscaPessoaFisica(pf);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <%@include file="../header.jsp"%>
</head>
<%@include file="../includes.jsp"%>
<%@include file="../navigation.jsp"%>
  <!-- Main content -->
<div class="col-md-6 col-centered">
     <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Adicionar Eventos</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" name="inserirUsuario" action="alteraPf.jsp" method="post">
                <div class="card-body">
                    <div class="row">
                        <input type="hidden" name="IDPessoa" value="<%=psf.getId()%>"/>
                        <div class="form-group col-md-6">
                            <label for="nome">Nome</label>
                              <input type="text" name="NOME" value="<%=psf.getNome()%>" class="form-control" id="nomeEvento" placeholder="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="EMAIL" value="<%=psf.getEmail()%>">
                        </div>
                    </div>
                    <div class="row">  
                        <div class="form-group col-md-4">
                          <label for="senha">Senha</label>
                          <input type="password" class="form-control" id="senha"  name="SENHA" value="">
                        </div>
                  
                        <div class="form-group col-md-4">
                           <label for="cpf">CPF</label>
                          <input type="text"  name="CPF" value="<%=psf.getCpf()%>" class="form-control" id="cpf" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="nasc">Data Nascimento</label>
                            <input id="nasc" type="text" name="DATANASC" value="<%=psf.getDatanasc()%>" class="form-control" placeholder="">
                        </div>
                    </div>                  
                    
                  <div class="form-group">
                    <label for="nasc">Genero : &nbsp; &nbsp; </label>  
                    <label>
                        <input type="radio" name="SEXO" class="minimal"  value= "masc">
                        Masculino
                    </label>
                    <label>
                        <input type="radio" name="SEXO" class="minimal" value= "fem">
                        Feminino
                    </label>
                  </div>  
                  
                  <div class="row">  
                    <div class="form-group col-md-6">
                        <label for="fone">Telefone</label>
                        <input type="text" name="FONE" value="<%=psf.getFone()%>" class="form-control" id="fone" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                         <label for="celfone">Celular</label>
                        <input type="text" name="CELFONE" value="<%=psf.getCelfone()%>" class="form-control" id="celfone" placeholder="">
                    </div>  
                 </div>
                 <div class="row">   
                        <div class="form-group col-md-6">
                           <label for="rua">Rua</label>
                           <input id="rua" type="text" name="RUA" value="<%=psf.getRua()%>" class="form-control"  placeholder="">
                        </div>  

                        <div class="form-group col-md-3">
                            <label for="numero">Numero</label>
                            <input id="numero" type="text" name="NUMERO" value="<%=psf.getNumero()%>" class="form-control" placeholder="">
                        </div>  

                      <div class="form-group col-md-3">
                         <label for="bairro">Bairro</label>
                         <input id="bairro" type="text" name="BAIRRO" value="<%=psf.getBairro()%>" class="form-control" placeholder="">
                      </div>    
                </div>
                <div class="row">    
                  <div class="form-group col-md-4">
                     <label for="cidade">Cidade</label>
                    <input  id="cidade" type="text" name="CIDADE" value="<%=psf.getCidade()%>" class="form-control" placeholder="">
                  </div>  
                    
                  <div class="form-group col-md-4">
                     <label for="uf">Estado</label>
                    <input id="uf" type="text" name="UF" value="<%=psf.getUf()%>" class="form-control" placeholder="">
                  </div>  
                    
                  <div class="form-group col-md-4">
                     <label for="cep">CEP</label>
                    <input id="cep" type="text" name="CEP" value="<%=psf.getCep()%>" class="form-control"  placeholder="">
                  </div>    
                  <input id="isperson" type="hidden" name="ISPERSON" value="fisica">  
                </div>
                </div>     
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button  onclick="location.href='../acesso/pagInicio.jsp'"  type="button"  class="btn btn-danger">cancelar</button>
                </div>
                
              </form>
            </div>
        </div>

<%@include file="../footer.jsp"%>
