<%@page import="br.com.sagilevents.bean.Usuario"%>
<%@page import="br.com.sagilevents.bean.PessoaJuridica"%>
<%@page import="br.com.sagilevents.controler.UsuarioControler"%>
<%@page import ="java.util.*"%>


<%
UsuarioControler usucont = new UsuarioControler();
Usuario usuLogado = (Usuario) session.getAttribute("UsuarioLogado");
PessoaJuridica pj = new PessoaJuridica (0,"","","","","","","","","","","","","","","","");
pj.setId(usuLogado.getId());
UsuarioControler usucount = new UsuarioControler();
PessoaJuridica psj = usucount.buscaPessoaJuridica(pj);
 

%>


<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>SAGIL Cadastro</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/jsp/public/css/materialize.css" type="text/css" rel="stylesheet"/>
  <link href="${pageContext.request.contextPath}/jsp/public/css/style.css" type="text/css" rel="stylesheet"/>

  <script type="text/javascript">
    $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: false // Close upon selecting a date,
          });
             
  </script>
</head>




<div class="container">
    <div class="section"></div>
      <main>
         <center>    
              <div class="row">
              <div class="col s12 m12 l6 offset-l3" >
            <form name="inserirUsuario" action="alteraPj.jsp" method="post" class="col s12" id="pj">
                            <div class="row">
                              <div class="input-field col s12">
                                  <i class="mdi-action-account-circle prefix"></i>
                                  <input type="hidden" name="IDPessoa" value="<%=psj.getId()%>"/>
                                  <input id="name3" type="text" name="NOME" value="<%=pj.getNome()%>">
                                   <label for="nome">Nome</label>
                              </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="email3" type="email" name = "EMAIL" value="<%=psj.getEmail()%>">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6 l6">
                                    <input id="password3" type="password" name="SENHA" value="">
                                    <label for="password">Senha</label>
                                </div>                                
                                <div class="input-field col s6 l6">
                                    <input id="password3" type="password" name="SENHA">
                                    <label for="password">Confirme senha</label>
                                </div>
                            </div>
                            <div class="row">
                              <div class="input-field col s12">
                              <input id="name3" type="text" name="RAZAOSOCIAL" value="<%=pj.getRazaosocial()%>">
                              <label for="first_name">Raz�o social da empresa</label>
                              </div>
                            </div>
                            <div class="row" style="visibility:hidden" id="ifYes">
                              <div class="input-field col s12">
                                  <input type="text" id="inscricao" name="INSCESTADUAL" value="<%=pj.getInscestadual()%>">
                                <label for="inscricao">Inscri��o Estadual</label>
                              </div>
                            </div>
                            <div class="row left">
                                <label style="font-size: 16px">Informa��es Tribut�rias:</label>                                
                                <input name="YESNO" type="radio" onclick="javascript:yesnoCheck();"  id="yesCheck"/>
                                <label for="yesCheck">Contrib. ICMS</label>
                                <input name="YESNO" type="radio" onclick="javascript:yesnoCheck();"  id="noCheck"/>
                                <label for="noCheck">N�o contribuinte</label>
                                 <input name="YESNO" type="radio" onclick="javascript:yesnoCheck();"  id="isento"/>
                                <label for="isento">Isento</label>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="cnpj" type="text" name="CNPJ" value="<%=pj.getCnpj()%>">
                                    <label for="cnpj">CNPJ</label>
                                </div>
                            </div>
                            
                            <div class="row">
                              <div class="input-field col s6">
                                <input id="fo" type="text" name="FONE" value="<%=pj.getFone()%>">
                                <label for="fone">Telefone contato</label>
                              </div>
                               <div class="input-field col s6">
                                <input id="celfone" type="text" name="CELFONE" value="<%=pj.getCelfone()%>">
                                <label for="celfone">Telefone celular</label>
                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="input-field col s6">
                                <input id="rua" type="text" name="RUA" value="<%=pj.getRua()%>">
                                <label for="email">Rua</label>
                              </div>
                               <div class="input-field col s2">
                                <input id="numero" type="text" name="NUMERO" value="<%=pj.getNumero()%>">
                                <label for="numero">Numero</label>
                              </div>
                              <div class="input-field col s4">
                                <input id="bairro" type="text" name="BAIRRO" value="<%=pj.getBairro()%>">
                                <label for="bairro">Bairro</label>
                              </div>
                                <div class="input-field col s6">
                                <input id="cidade" type="text" name="CIDADE" value="<%=pj.getCidade()%>">
                                <label for="cidade">Cidade</label>
                              </div>
                              <div class="input-field col s2">
                                <input id="uf" type="text" name="UF" value="<%=pj.getUf()%>">
                                <label for="uf">UF</label>
                              </div>
                              <div class="input-field col s4">
                                <input id="cep" type="text" value="<%=pj.getCep()%>" name="CEP">
                                <label for="cep">CEP</label>
                              </div>
                                <div>
                                <input id="isperson" type="hidden" name="ISPERSON" value="juridica">
                              </div>
                            </div>
                              
                           
                            
                            <div class="row">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button class="btn cyan waves-effect waves-light right" type="submit" name="Enviar">OK
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>
              </div>
              </div>
         </center>
      </main>
</div>






