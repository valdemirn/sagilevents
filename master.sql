-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: master
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `ideventos` int(11) NOT NULL AUTO_INCREMENT,
  `nome_evento` varchar(45) DEFAULT NULL,
  `email_evento` varchar(45) DEFAULT NULL,
  `data_inicio_evento` varchar(10) DEFAULT NULL,
  `data_fim_evento` varchar(10) DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  `id_local` int(11) DEFAULT NULL,
  PRIMARY KEY (`ideventos`,`usuarios_id`),
  KEY `fk_eventos_usuarios_idx` (`usuarios_id`),
  KEY `fk_id_local` (`id_local`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES (1,'III INTERNATIONAL CONFERENCE ON BALANCE DISTU','spadkins@comcast.net','29/9/11','1/10/11',100,0),(2,'REUNIÃO ABBOTT VASCULAR – CHILE','stecoop@comcast.net','01/9/11','5/9/11',100,0),(3,'A ENDOSCOPIA NA DII','mschilli@icloud.com','11/9/11','12/9/11',100,0),(4,'DOENÇA DE CROHN: DISCUSSÃO DE CASOS CLÍNICOS','rtanter@me.com','11/10/11','11/10/11',100,0),(5,'EVENTO ONG PACIENTES','devphil@yahoo.ca','20/10/11','22/10/11',100,0),(6,'TOWN HALL','burniske@optonline.net','30/11/11','30/11/11',100,0),(7,'CONGRESSO MINEIRO DE PNEUMOLOGIA E CIRURGIA T','dgatwood@me.com','11/8/11','13/8/11',100,0),(8,'TIME MANAUS','gward@yahoo.ca','17/9/11','17/9/11',100,0),(9,'CURSO DE CAPACITAÇÃO EM AVC – CURITIBA','zeller@att.net','29/10/11','29/10/11',100,0),(10,'III SIMPÓSIO NORTE-NORDESTE DE ATUALIZAÇÃO EM','tlinden@live.com','12/8/11','13/8/11',100,0),(11,'DOENÇA DE CROHN: VISÃO ATUAL DO GASTRO E DO C','shaffei@yahoo.com','26/8/11','28/8/11',100,0),(12,'DOENÇAS INFLAMATÓRIAS INTESTINAIS – SÃO LUIS','tmaek@optonline.net','16/9/11','16/9/11',100,0),(13,'VIAGEM DE INCENTIVO – GERENTE DE VENDAS','gfody@sbcglobal.net','1/1/12','1/2/12',100,0),(14,'SEGURO VIAGEM – INDIVIDUAL ','bmorrow@msn.com','23/9/11','8/9/11',100,0),(15,'INDIVIDUAL PARIS – PROPOSTA','mrobshaw@yahoo.ca','25/12/11','1/1/12',100,0),(16,'CASAL – OSLO – NORUEGA','slanglois@att.net','2/5/12','6/5/12',100,0),(17,'TRANSFER + SEGURO VIAGEM ORLANDO','dinther@me.com','13/8/11','20/8/11',100,0),(18,'FAMÍLIA – EUROPA – OUTUBRO ','henkp@hotmail.com','8/10/11','17/10/11',100,0),(19,'TRANSFER E SEGURO EUA','sokol@sbcglobal.net','6/2/12','22/2/12',100,0),(20,'TOUR EM SP','kludge@msn.com','20/8/11','20/8/11',100,0),(21,'JANTAR CAMPANHA DE INVERNO – TRANSFER SANTOS','cumarana@outlook.com','28/8/11','28/8/11',100,0),(22,'JANTAR CAMPANHA DE INVERNO – TRANSFER SÃO JOS','klaudon@gmail.com','28/8/11','28/8/11',100,0),(23,'BUSINESS REVIEW – REVISÃO PLANOS KEY ACCOUNT','kdawson@yahoo.ca','21/9/11','22/9/11',100,0),(24,'ECCO 2012','rsmartin@icloud.com','15/8/11','29/8/11',100,0),(25,'LEADING THROUGHTCHANGE IN IBD 2012','arachne@live.com','15/8/11','29/8/11',100,0),(26,'IBD AHEAD 2011 INTERNATIONAL MEETING','bader@gmail.com','15/7/11','22/7/11',100,0),(27,'DDW 2012','pkplex@optonline.net','24/8/11','8/9/11',100,0),(28,'E-HOLIDAYS','kingjoshi@mac.com','17/2/12','21/2/12',100,0),(29,'VIAGEM DE INCENTIVO GUERRA','calin@msn.com','19/2/11','26/2/11',100,0),(30,'66º CONGRESSO BRASILEIRO DE CARDIOLOGIA','ewaters@live.com','16/9/11','19/9/11',100,0),(31,'SCANIA – CONSTRUCTION TEST & DRIVE','bwcarty@optonline.net','25/11/11','2/12/11',100,0),(32,'CONVENÇÃO EPD','miltchev@icloud.com','6/2/12','10/2/12',100,0),(33,'VIAGEM FAMÍLIA LOUREIRO – LAS VEGAS','sblack@comcast.net','9/10/11','15/10/11',100,0),(34,'VIAGEM FAMÍLIA LOUREIRO – NYC','satch@att.net','9/10/11','14/10/11',100,0),(35,'LANÇAMENTO VM','naupa@yahoo.ca','04/10/2011','05/10/2011',100,0),(36,'SKOLKOVO PROGRAMME IN BRAZIL','rogerspl@comcast.net','6/11/11','14/11/11',100,0),(37,'TREINAMENTO ABBOTT VASCULAR','gator@mac.com','15/8/11','19/8/11',100,0),(38,'REUNIÃO DE PROJETO ABBOTT','alastair@hotmail.com','17/8/11','17/8/11',100,0),(39,'XII JORNADA DE GASTROENTEROLOGIA CLÍNICA, CIR','ghaviv@live.com','19/8/11','20/8/11',100,0),(40,'ENCONTRO GRUPO DE DISCUSSÃO DE CASOS CLÍNICOS','pereinar@aol.com','25/8/11','25/8/11',100,0),(41,'JORNADA ESTADUAL DE GASTROENTEROLOGIA ','stakasa@yahoo.ca','25/8/11','27/8/11',100,0),(42,'TRATAMENTO DAS DII\'S COM TERAPIA BIOLÓGICA','doormat@outlook.com','30/8/11','30/8/11',100,0),(43,'VII SIMPÓSIO PRONTOCARDIO DE DOENÇA CORONÁRIA','houle@yahoo.com','2/9/11','3/9/11',100,0),(44,'REEMISSÃO LIVRE DE CORICÓIDES NA DOENÇA DE CR','paley@gmail.com','15/9/11','15/9/11',100,0),(45,'TERAPIA BIOLÓGICA NA DOENÇA DE CROHN','alfred@me.com','16/9/11','16/9/11',100,0),(46,'GASTRO RECIFE 2011','sumdumass@yahoo.com','22/9/11','24/9/11',100,0),(47,'AGENTE FAZ','scottzed@gmail.com','10/3/12','10/3/12',100,0),(48,'TREINAMENTO A.V. BRASIL','madanm@icloud.com','2/9/11','2/9/11',100,0),(49,'TREINAMENTO A.V. BRASIL','gregh@comcast.net','3/10/11','3/10/11',100,0),(50,'TREINAMENTO A.V. BRASIL','drhyde@icloud.com','7/11/11','7/11/11',100,0),(51,'TREINAMENTO A.V. BRASIL','spadkins@comcast.net','2/12/11','2/12/11',100,0),(52,'VI SIMPÓSIO INTERNACIONAL MULTIDICIPLINAR DE ','stecoop@comcast.net','4/11/11','5/11/11',100,0),(53,'TREINAMENTO EPD','mschilli@icloud.com','5/11/11','11/11/11',101,0),(54,'IBD SUMMIT GASTRO','rtanter@me.com','6/10/11','8/10/11',101,0),(55,'REUNIÃO DE CLIENTES II','dgatwood@me.com','21/9/11','22/9/11',101,0),(56,'EVENTO MAPOR','gward@yahoo.ca','24/9/11','26/9/11',101,0),(57,'EVENTO MAPOR','zeller@att.net','1/10/11','3/10/11',101,0),(58,'HONEY MOON - LOLLO & ELI','tlinden@live.com','18/2/12','2/3/12',101,0),(59,'JANTAR ABBOTT VASCULAR','shaffei@yahoo.com','12/9/11','12/10/11',101,0),(60,'HOSPEDAGEM JOSE PEPPE','tmaek@optonline.net','12/9/11','15/9/11',101,0),(61,'CONVENÇÃO DE VENDAS PPD','temmink@optonline.net','4/3/12','9/3/12',101,0),(62,'JANTAR CAMPANHA DE INVERNO HSBC BRASIL – TRAN','gfody@sbcglobal.net','28/8/11','28/8/11',101,0),(63,'BUSINESS REVIEW – REVISÃO PLANOS KEY ACCOUNT','bmorrow@msn.com','21/9/11','22/9/11',101,0),(64,'EUROPEAN AIDS CONFERENCE','mrobshaw@yahoo.ca','12/10/11','15/10/11',101,0),(65,'CROSSROAD -CLINICAL AND PRATICE ASPECTS OF CO','slanglois@att.net','10/10/11','11/10/11',101,0),(66,'VIAGEM FAMÍLIA ROJAS – LAS VEGAS / LOS ANGELE','dinther@me.com','9/11/11','20/11/11',101,0),(67,'GRUPO VOLVO VIP – LISBOA','henkp@hotmail.com','1/12/11','6/12/11',101,0),(68,'CAMPANHA DE RELACIONAMENTO PCO','sokol@sbcglobal.net','28/11/11','4/12/11',101,0),(69,'BRAND TEAM LEADERS TRAINIG','kludge@msn.com','11/10/11','14/10/11',101,0),(70,'67º CONGRESSO BRASILEIRO DE DERMATOLOGIA','cumarana@outlook.com','31/8/12','4/9/12',101,0),(71,'FORMULA 1','klaudon@gmail.com','25/11/11','27/11/11',101,0),(72,'XXIX CONGRESSO BRASILEIRO DE REUMATOLOGIA ','kdawson@yahoo.ca','19/9/12','22/9/12',101,0),(73,'CONVENÇÃO VOLVO 2012','arachne@live.com','9/3/12','13/3/12',101,0),(74,'CONVENÇÃO DANONE 2012','bader@gmail.com','1/4/12','5/4/12',101,0),(75,'CONVENÇÃO ABBOTT VASCULAR','pkplex@optonline.net','13/1/12','20/1/12',101,0),(76,'PALESTRA BOEHRINGER','kingjoshi@mac.com','8/10/12','8/10/12',101,0),(77,'AAD 2012','calin@msn.com','16/3/12','20/03/212',101,0),(78,'3RD WORLD PSORIASIS & PSORIATIC ARTHRITIS CON','ewaters@live.com','27/6/12','1/7/12',101,0),(79,'21 EUROPEAN ACADEMY OF DERMATOLOGY AND VENERO','bwcarty@optonline.net','6/9/12','9/9/12',101,0),(80,'QUALY REPRESENTANTES DIAMANTES 2012','miltchev@icloud.com','1/10/12','5/10/12',101,0),(81,'QUALY REVENDAS DIAMANTES 2012','sblack@comcast.net','1/5/12','5/5/12',101,0),(82,'CAMPANHA BIG APPLE','satch@att.net','1/3/12','4/3/12',101,0),(83,'CONVENÇÃO VOLVO 2012','naupa@yahoo.ca','3/9/11','13/03/2012',101,0),(84,'REUNIÃO PÓS ENV DA EQUIPE MAPOR - ','rogerspl@comcast.net','26/9/11','30/9/11',101,0),(85,'DOENÇA INFLAMATÓRIA NA PRÁTICA CLÍNICA','gator@mac.com','15/9/11','15/9/11',101,0),(86,'CHEST – HONOLULU','alastair@hotmail.com','22/10/12','26/10/12',101,0),(87,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','ghaviv@live.com','17/10/11','18/10/11',101,0),(88,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','pereinar@aol.com','18/10/11','19/10/11',101,0),(89,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','stakasa@yahoo.ca','19/10/11','20/10/11',101,0),(90,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','doormat@outlook.com','20/10/11','21/10/11',101,0),(91,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','houle@yahoo.com','24/10/11','25/10/11',101,0),(92,'WORKSHOP VET – LANCAMENTO CAMPANHA MYCOFLEX','paley@gmail.com','25/10/11','26/10/11',101,0),(93,'VISITA A FABRICA DA ABBOTT EM SAN DIEGO','alfred@me.com','16/10/11','20/10/11',101,0),(94,'CONGRESSO DE VIDEOCIRURGIA – 1º SBCBM-SP E O ','sumdumass@yahoo.com','30/9/11','1/10/11',101,0),(95,'JORNADA SOBRE TERAPIA BIOLÓGICA E STE UP ACEL','scottzed@gmail.com','20/10/11','20/10/11',101,0),(96,'SIMPÓSIRO CSV 2011','madanm@icloud.com','8/10/11','8/10/11',101,0),(97,'E-HOLIDAYS (QUOTE APRIL 2012)','gregh@comcast.net','1/4/11','7/4/11',101,0),(98,'SHIKOLAT - BRAZIL FEB 2012','drhyde@icloud.com','17/2/12','29/2/12',101,0),(99,'FABRICA DE CAMINHÕES','','27/09/2011','28/09/2011',101,0),(101,'HOSPEDAGEM ABBOTT VASCULAR','stecoop@comcast.net','19/9/11','23/9/11',101,0),(102,'CONVENÇÃO DE VENDAS 2012','mschilli@icloud.com','06/02/2011','10/02/2012',101,0),(103,'CONGRESSO BRASILEIRO DE CIRURGIA DO JOELHO','rtanter@me.com','29/3/11','31/3/11',101,0),(104,'INTERNATIONAL SYMPOSIUM ON KNEE TRAUMA','devphil@yahoo.ca','28/6/11','30/6/11',101,0),(105,'REUNIÃO ABBOTT SP','burniske@optonline.net','30/10/11','1/11/11',101,0),(106,'FÓRUM DE ATUALIZAÇÃO IVC E DO IAM -01 DE OUTU','dgatwood@me.com','30/09/2011','01/10/2011',101,0),(107,'PEUGEOT CITROEN DO BRASIL','gward@yahoo.ca','24/10/11','30/10/11',101,0),(108,'SHOW RURAL COOPAVEL','zeller@att.net','6/2/12','10/2/12',101,0),(109,'EXPODIRETO COTRIJAL','tlinden@live.com','5/3/12','9/3/12',101,0),(110,'AGRISHOW','shaffei@yahoo.com','30/4/12','4/5/11',101,0),(111,'SEGURO VIAGEM – INDIVIDUAL ','tmaek@optonline.net','28/9/11','16/10/11',101,0),(112,'VIAGEM EXECUTIVOS','temmink@optonline.net','16/10/11','23/10/11',101,0),(113,'ENCONTRO PARA AUDITORES, MEDICINA BASEADA EM ','gfody@sbcglobal.net','11/11/11','12/11/11',101,0),(114,'JANTAR TOW HALL','bmorrow@msn.com','20/9/11','20/9/11',101,0),(115,'CONGRESSO BRASILEIRO DE CIRURGIA DO JOELHO','slanglois@att.net','13/10/2011','15/10/2011',101,0),(117,'REUNIÃO EM MIAMI','henkp@hotmail.com','5/10/11','7/10/11',101,0),(118,'CARTES NORT AMERICA','sokol@sbcglobal.net','5/3/11','7/3/11',101,0),(119,'REUNIÕES REGIONAIS','kludge@msn.com','4/10/11','20/10/11',101,0),(120,'EVENTO BOEHRINGER MONTE AZUL','cumarana@outlook.com','17/1/12','17/1/12',101,0),(121,'TREINAMENTO ABBOTT VASCULAR - I','klaudon@gmail.com','2/10/11','5/10/11',101,0),(122,'TREINAMENTO ABBOTT VASCULAR – II','kdawson@yahoo.ca','16/10/11','19/10/11',101,0),(123,'TREINAMENTO ABBOTT VASCULAR - III','rsmartin@icloud.com','19/10/11','21/10/11',101,0),(124,'TREINAMENTO ABBOTT VASCULAR - IV','arachne@live.com','20/11/11','23/11/11',101,0),(125,'TREINAMENTO ABBOTT VASCULAR - V','bader@gmail.com','23/11/11','25/11/11',101,0),(126,'IX FORUM DE FARMACOECONOMIA E GESTAO EM ONCOL','pkplex@optonline.net','23/3/12','25/3/12',101,0),(127,'WINTER RHEUMATOLOGY SYMPOSIUM','kingjoshi@mac.com','27/1/12','3/2/12',101,0),(128,'FORD FABRICA CAMAÇARI','calin@msn.com','27/10/11','28/10/11',101,0),(129,'THE HARBOUR REPORT 2012','ewaters@live.com','10/11/11','10/11/11',101,0),(130,'SUMMIT MSD','bwcarty@optonline.net','9/3/12','11/3/12',101,0),(131,'INCENTIVO CIRQUE SOLEIL RJ','miltchev@icloud.com','13/12/12','15/12/12',101,0),(132,'CURSO ESPECIAL – REGULAMENTAÇÃO DO TRANSPORTE','sblack@comcast.net','26/11/11','26/11/11',101,0),(133,'61º CONGRESSO BRASILEIRO DE COLOPROCTOLOGIA ','satch@att.net','4/9/12','8/9/12',101,0),(134,'V LATIN AMERICAN LUNG CANCER CONFERENCE (LALC','rogerspl@comcast.net','25/7/12','27/7/12',101,0),(135,'SOGESP – XVII CONGRESSO PAULISTA DE OBSTETRIC','gator@mac.com','30/8/12','1/9/12',101,0),(136,'XIX CONGRESSO BRASILEIRO DE CANCEROLOGIA (CON','ghaviv@live.com','24/10/12','27/10/12',101,0),(137,'XXV CONGRESSO BRASILEIRO DE NEUROLOGIA ','doormat@outlook.com','4/8/12','8/8/12',101,0),(138,'WEBMEETING – TB EM PACIENTES COM ARTRITE REUM','madanm@icloud.com','23/11/11','24/11/11',101,0),(139,'WEBMEETING DE AIJ','gregh@comcast.net','30/11/11','30/11/11',101,0),(140,'AUA ANNUAL MEETING - AMERICAN UROLOGICAL ASSO','drhyde@icloud.com','18/5/12','23/5/12',101,0);
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local`
--

DROP TABLE IF EXISTS `local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local` (
  `id_local` int(11) NOT NULL,
  `tipo_local` varchar(45) DEFAULT NULL,
  `nome_local` varchar(255) NOT NULL,
  `rua_local` varchar(100) DEFAULT NULL,
  `numero_local` varchar(5) DEFAULT NULL,
  `bairro_local` varchar(100) DEFAULT NULL,
  `cep_local` varchar(10) DEFAULT NULL,
  `cidade_local` varchar(50) DEFAULT NULL,
  `estado_local` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_local`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local`
--

LOCK TABLES `local` WRITE;
/*!40000 ALTER TABLE `local` DISABLE KEYS */;
/*!40000 ALTER TABLE `local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoajuridica`
--

DROP TABLE IF EXISTS `pessoajuridica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoajuridica` (
  `idpessoajuridica` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `razaosocial` varchar(255) DEFAULT NULL,
  `inscestadual` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fone` varchar(255) DEFAULT NULL,
  `celfone` varchar(255) DEFAULT NULL,
  `rua` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `isperson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpessoajuridica`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoajuridica`
--

LOCK TABLES `pessoajuridica` WRITE;
/*!40000 ALTER TABLE `pessoajuridica` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoajuridica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `data` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `stattus` varchar(255) DEFAULT NULL,
  `fone` varchar(255) NOT NULL,
  `celfone` varchar(255) NOT NULL,
  `rua` varchar(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(255) NOT NULL,
  `cep` varchar(255) NOT NULL,
  `isperson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas`
--

LOCK TABLES `pessoas` WRITE;
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT INTO `pessoas` VALUES (37,101,'Jane Doe','176.181.658-66','20/08/2012',NULL,'jane@master.com.br',NULL,'(11) 2633-9000','(11) 2633-9000','Rua Vergueiro','235/249','Liberdade',' S?o Paulo','SP','04561006','fisica'),(36,100,'John Doe','321.005.624-28','09/06/1992',NULL,'johndoe@master.com.br',NULL,'(11) 2633-9229','(11) 3823-9000','Rua Amador Bueno','389/491','Santo Amaro','S?o Paulo','SP','','fisica');
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `isperson` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (100,'John Doe','johndoe@master.com.br','123456','ativo','johndoe@master.com.br','user','fisica'),(101,'Jane Doe','jane@master.com.br','123456','ativo','jane@master.com.br','ADM','fisica');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-13 15:17:49
